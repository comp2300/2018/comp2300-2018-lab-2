# COMP2300 lab 2 template

<https://cs.anu.edu.au/courses/comp2300/labs/02-first-machine-code/>

This is the lab 2 template for writing pure-asm code programs for your
discoboard comp2300.

Note: since there were some VSCode/PlatformIO shenanigans in week 1, we've made
a change to the template and COMP2300 VSCode extension this time. Make sure
you've updated your extension---and everything should just work (although you
*might* have to download & unpack a bit more PlatformIO stuff, which I realise
takes a while---sorry about that).

If you have any questions, ask them on
[Piazza](https://piazza.com/anu.edu.au/spring2018/comp23006300/home).
